#!/bin/bash

#${array[$((${#array[@]}-1))]}
array=(1)

	#Blev ved med at tjekke til tilfreds.
 	while true; do 

 		#Tjek hvis sidste element er større end vores input.
		if (($1 < ${array[$((${#array[@]}-1))]} )); then 
			#Hvis større end input, så stop.
			break;
		else 

			length=${#array[@]}
			#Ellers udregn næste element
			for ((i=0; i<length;i++)); do

				#Det næste element regnes, ved sum_{i=0}^{n} c_{i}*c_{n-i}
				((array[length] +=  array[i] * array[length-i-1]))
			done
		fi

	done

#Hvis det sidste catalan tal er større end input, så slet det.
if ((array[${#array[@]}-1] > $1)); then
	unset array[${#array[@]}-1];
fi

#Print tallene
echo ${array[@]};
